import React from "react";
import Avatar from "react-avatar";
import "./Story.css";

const Story = ({ image, profileSrc, title }) => {
  return (
    <div style={{ backgroundImage: `url(${image})` }} className="story">
      <Avatar
        className="story-avatar"
        src={profileSrc}
        round={true}
        size="40"
      />
      <h4>{title}</h4>
    </div>
  );
};

export default Story;
