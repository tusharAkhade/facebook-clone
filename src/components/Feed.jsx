import React, { useEffect, useState } from "react";
import "./Feed.css";
import MessageSender from "./MessageSender";
import Post from "./Post";
import StoryReel from "./StoryReel";
import { collection, getDocs, onSnapshot } from "firebase/firestore";
import db from "../firebase";

const Feed = () => {
  const usersCollectionRef = collection(db, "posts");

  const [posts, setPosts] = useState([]);

  useEffect(() => {
    console.log(usersCollectionRef);
    // const getUsers = async () => {
    //   const data = await getDocs(usersCollectionRef);
    //   setPosts(data.docs.map((doc) => ({ id: doc.id, data: doc.data() })));
    // };
    const getUsers = () => {
      onSnapshot(usersCollectionRef, (snapshot) => {
        setPosts(
          snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }))
        );
      });
    };
    getUsers();
  }, []);

  return (
    <div className="feed">
      <StoryReel />
      <MessageSender />

      {posts?.map((post) => (
        <Post
          key={post.id}
          profilePic={post.data.profilePic}
          message={post.data.message}
          timestamp={post.data.timestamp}
          username={post.data.username}
          image={post.data.image}
        />
      ))}
    </div>
  );
};

export default Feed;
