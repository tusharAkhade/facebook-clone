import React from "react";
import Avatar from "react-avatar";
import {
  MdAccountCircle,
  MdChatBubbleOutline,
  MdNearMe,
  MdOutlineExpandMore,
  MdThumbUp,
} from "react-icons/md";
import "./Post.css";

const Post = ({ profilePic, image, username, timestamp, message }) => {
  return (
    <div className="post">
      <div className="post-top">
        <Avatar
          src={profilePic}
          className="post-avatar"
          round={true}
          size="40"
        />
        <div className="post-topInfo">
          <h3>{username}</h3>
          <p>{new Date(timestamp?.toDate()).toUTCString()}</p>
        </div>
      </div>

      <div className="post-bottom">
        <p>{message}</p>
      </div>

      <div className="post-image">
        <img src={image} alt="" />
      </div>

      <div className="post-options">
        <div className="post-option">
          <MdThumbUp size="20" />
          <p>Like</p>
        </div>
        <div className="post-option">
          <MdChatBubbleOutline size="20" />
          <p>Comment</p>
        </div>
        <div className="post-option">
          <MdNearMe size="20" />
          <p>Share</p>
        </div>
        <div className="post-option">
          <MdAccountCircle size="20" />
          <MdOutlineExpandMore size="20" />
        </div>
      </div>
    </div>
  );
};

export default Post;
