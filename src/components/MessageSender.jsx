import React, { useState } from "react";
import Avatar from "react-avatar";
import { MdInsertEmoticon, MdPhotoLibrary, MdVideocam } from "react-icons/md";
import "./MessageSender.css";
import { useStateValue } from "./StateProvider";
import db from "../firebase";
import {
  addDoc,
  collection,
  FieldValue,
  serverTimestamp,
} from "firebase/firestore";

const MessageSender = () => {
  const [{ user }, dispatch] = useStateValue();
  const usersCollectionRef = collection(db, "posts");

  const [input, setInput] = useState("");
  const [imageUrl, setImageUrl] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    await addDoc(usersCollectionRef, {
      message: input,
      profilePic: user.photoURL,
      username: user.displayName,
      image: imageUrl,
      timestamp: serverTimestamp(),
    });
    setInput("");
    setImageUrl("");
  };
  return (
    <div className="messageSender">
      <div className="messageSender-top">
        <Avatar src={user.photoURL} round={true} size="40" />
        <form>
          <input
            value={input}
            onChange={(e) => setInput(e.target.value)}
            className="messageSender-input"
            placeholder={`Whats on your mind, ${user.displayName}?`}
          />
          <input
            value={imageUrl}
            onChange={(e) => setImageUrl(e.target.value)}
            placeholder="image URL (Optional)"
          />
          <button onClick={handleSubmit}>Handle Submit</button>
        </form>
      </div>

      <div className="messageSender-bottom">
        <div className="messageSender-option">
          <MdVideocam color="red" size="20" />
          <h3>Live Video</h3>
        </div>
        <div className="messageSender-option">
          <MdPhotoLibrary color="green" size="20" />
          <h3>Photo/Video</h3>
        </div>
        <div className="messageSender-option">
          <MdInsertEmoticon color="orange" size="20" />
          <h3>Feeling/Activity</h3>
        </div>
      </div>
    </div>
  );
};

export default MessageSender;
