import React from "react";
import Avatar from "react-avatar";
import "./SidebarRow.css";

const SidebarRow = ({ src, Icon, title }) => {
  return (
    <div className="sidebarRow">
      {src && <Avatar round={true} size="40" src={src} />}
      {Icon && <Icon className="icon" />}
      <h4>{title}</h4>
    </div>
  );
};

export default SidebarRow;
