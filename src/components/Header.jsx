import React from "react";
import "./Header.css";
import { FaFlag, FaHome, FaSearch } from "react-icons/fa";
import {
  MdOutlineSubscriptions,
  MdOutlineStorefront,
  MdSupervisedUserCircle,
  MdAdd,
  MdForum,
  MdNotificationsActive,
  MdExpandMore,
} from "react-icons/md";
import Avatar from "react-avatar";
import { useStateValue } from "./StateProvider";

const Header = () => {
  const [{ user }, dispatch] = useStateValue();

  return (
    <div className="header">
      <div className="header-left">
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Facebook_f_logo_%282019%29.svg/214px-Facebook_f_logo_%282019%29.svg.png"
          alt="Facebook Logo"
        />
        <div className="header-input">
          <FaSearch />
          <input placeholder="Search Facebook" type="text" />
        </div>
      </div>

      <div className="header-center">
        <div className="header-option header-option-active">
          <FaHome className="icon" size="20px" />
        </div>
        <div className="header-option">
          <FaFlag className="icon" size="20px" />
        </div>
        <div className="header-option">
          <MdOutlineSubscriptions className="icon" size="20px" />
        </div>
        <div className="header-option">
          <MdOutlineStorefront className="icon" size="20px" />
        </div>
        <div className="header-option">
          <MdSupervisedUserCircle className="icon" size="20px" />
        </div>
      </div>
      <div className="header-right">
        <div className="header-info">
          <Avatar src={user.photoURL} round={true} size="40" />
          <h4>{user.displayName}</h4>
        </div>
        <MdAdd className="icon" size="20px" color="gray" />
        <MdForum className="icon" size="20px" color="gray" />
        <MdNotificationsActive className="icon" size="20px" color="gray" />
        <MdExpandMore className="icon" size="20px" color="gray" />
      </div>
    </div>
  );
};

export default Header;
