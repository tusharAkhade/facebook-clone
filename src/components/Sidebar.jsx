import React from "react";
import {
  MdChat,
  MdEmojiFlags,
  MdLocalHospital,
  MdOutlineExpandMore,
  MdPeople,
  MdStorefront,
  MdVideoLibrary,
} from "react-icons/md";
import "./Sidebar.css";
import SidebarRow from "./SidebarRow";
import { useStateValue } from "./StateProvider";

const Sidebar = () => {
  const [{ user }, dispatch] = useStateValue();

  return (
    <div className="sidebar">
      <SidebarRow src={user.photoURL} title={user.displayName} />
      <SidebarRow Icon={MdLocalHospital} title="COVID-19 Information Center" />
      <SidebarRow Icon={MdEmojiFlags} title="Pages" />
      <SidebarRow Icon={MdPeople} title="Friends" />
      <SidebarRow Icon={MdChat} title="Messenger" />
      <SidebarRow Icon={MdStorefront} title="Marketplace" />
      <SidebarRow Icon={MdVideoLibrary} title="Videos" />
      <SidebarRow Icon={MdOutlineExpandMore} title="See more" />
    </div>
  );
};

export default Sidebar;
