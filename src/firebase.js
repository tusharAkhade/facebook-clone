import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyB_6D2DU23kYvWOJTl-ul6JbogWiR_vAJg",
  authDomain: "facebook-clone-fe14b.firebaseapp.com",
  projectId: "facebook-clone-fe14b",
  storageBucket: "facebook-clone-fe14b.appspot.com",
  messagingSenderId: "951839112545",
  appId: "1:951839112545:web:d42f2354fcaa4ba2681920",
  measurementId: "G-L44C6LB56J",
};

const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth(firebaseApp);

const provider = new GoogleAuthProvider();

export { auth, provider };
export default db;
